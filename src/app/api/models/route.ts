import { NextRequest, NextResponse } from "next/server";

export async function GET(req: NextRequest): Promise<NextResponse> {
  try {
    const baseUrl = process.env.VLLM_URL;
    const apiKey = process.env.API_KEY;
    const headers = new Headers();
    if (!baseUrl) {
      throw new Error("VLLM_URL is not set");
    }

    headers.set("Content-Type", "application/json");
    if (apiKey !== undefined) {
      headers.set("Authorization", `Bearer ${apiKey}`);
    }
    const res = await fetch(baseUrl + "/v1/models",{
                            headers: headers
                    });
    if (res.status !== 200) {
      const statusText = res.statusText;
      const responseBody = await res.text();
      console.error(`vLLM /api/models response error: ${responseBody}`);
      return NextResponse.json(
        {
          success: false,
          error: statusText,
        },
        { status: res.status }
      );
    }
    return new NextResponse(res.body, res);
  } catch (error) {
    console.error(error);
    return NextResponse.json(
      {
        success: false,
        error: error instanceof Error ? error.message : "Unknown error",
      },
      { status: 500 }
    );
  }
}
